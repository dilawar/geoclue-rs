// https://gitlab.freedesktop.org/geoclue/geoclue/-/blob/master/demo/where-am-i.c?ref_type=heads

use clap::Parser;
use geoclue::prelude::*;

#[derive(Parser)]
struct Opt {
    #[arg(short, long, default_value_t = 8)]
    accuracy_level: u8,
    #[arg(short = 'i', long, default_value_t)]
    time_threshold: u32,
    #[arg(short, long, default_value_t = 30)]
    timeout: u32,
}

static MAIN_LOOP: std::sync::OnceLock<glib::MainLoop> = std::sync::OnceLock::new();

fn main() {
    let opt = Opt::parse();

    glib::timeout_add_seconds(opt.timeout, on_location_timeout);

    geoclue::Simple::new_with_thresholds(
        "geoclue-where-am-i",
        accuracy_level(opt.accuracy_level),
        opt.time_threshold,
        0,
        gio::Cancellable::NONE,
        on_simple_ready,
    );

    let main_loop = MAIN_LOOP.get_or_init(|| {
        glib::MainLoop::new(None, false)
    });
    main_loop.run();
}

fn on_simple_ready(res: Result<geoclue::Simple, glib::Error>) {
    let simple = match res {
        Ok(simple) => simple,
        Err(err) => panic!("Failed to connect to GeoClue2 service: {}", err),
    };

    if let Some(client) = simple.client() {
        //println!("Client object: {}", client.object_path());

        client.connect_active_notify(on_client_active_notify);
    }

    print_location(&simple);
}

fn accuracy_level(level: u8) -> geoclue::AccuracyLevel {
    use geoclue::AccuracyLevel::*;

    match level {
        1 => Country,
        4 => City,
        5 => Neighborhood,
        6 => Street,
        8 => Exact,
        _ => panic!("Invalid accuracy level: '{}'", level),
    }
}

fn print_location(simple: &geoclue::Simple) {
    if let Some(location) = simple.location() {
        println!("\nNew location:");

        println!("Latitude:    {}°\nLongitude:   {}°\nAccuracy:    {} meters",
            location.latitude(),
            location.longitude(),
            location.accuracy(),
        );

        let altitude = location.altitude ();
        if altitude != -f64::MAX {
            println!("Altitude:    {altitude} meters");
        }

        let speed = location.speed();
        if speed >= 0. {
            println!("Speed:       {speed} meters/second");
        }

        let heading = location.heading();
        if heading >= 0. {
            println!("Heading:     {heading}°\n");
        }

        if let Some(desc) = location.description() {
            println!("Description: {desc}");
        }

        if let Some(timestamp) = location.timestamp() {
            let (sec, _) = timestamp.get::<(u64, u64)>().unwrap();
            let date_time = glib::DateTime::from_unix_local(sec as i64).unwrap();
            let str = date_time.format("%c (%s seconds since the Epoch)").unwrap();

            println!("Timestamp:   {str}");
        }
    }
}

fn on_client_active_notify(client: &geoclue::ClientProxy) {
    if client.is_active() {
        return;
    }

    println!("Geolocation disabled. Quitting..");
    on_location_timeout();
}

fn on_location_timeout() -> glib::ControlFlow {
    let main_loop = MAIN_LOOP.get_or_init(|| {
        glib::MainLoop::new(None, false)
    });
    main_loop.quit();

    glib::ControlFlow::Break
}
