<!-- file * -->
<!-- enum AccuracyLevel -->
<!-- struct Client -->
Abstract interface type for the D-Bus interface <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Client.top_of_page">org.freedesktop.GeoClue2.Client`</link>`.

# Implements

[`ClientExt`][trait@crate::prelude::ClientExt]
<!-- trait ClientExt -->
Trait containing all [`struct@Client`] methods.

# Implementors

[`ClientProxy`][struct@crate::ClientProxy], [`Client`][struct@crate::Client]
<!-- impl Client::fn interface_info -->
Gets a machine-readable description of the <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Client.top_of_page">org.freedesktop.GeoClue2.Client`</link>` D-Bus interface.

# Returns

A [`gio::DBusInterfaceInfo`][crate::gio::DBusInterfaceInfo]. Do not free.
<!-- impl Client::fn override_properties -->
Overrides all `GObject` properties in the [`Client`][crate::Client] interface for a concrete class.
The properties are overridden in the order they are defined.
## `klass`
The class structure for a `GObject` derived class.
## `property_id_begin`
The property id to assign to the first overridden property.

# Returns

The last property id.
<!-- trait ClientExt::fn call_start -->
Asynchronously invokes the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Client.Start">Start()`</link>` D-Bus method on `self`.
When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
You can then call `gclue_client_call_start_finish()` to get the result of the operation.

See [`call_start_sync()`][Self::call_start_sync()] for the synchronous, blocking version of this method.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the request is satisfied or [`None`].
<!-- trait ClientExt::fn call_start_finish -->
Finishes an operation started with [`call_start()`][Self::call_start()].
## `res`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to [`call_start()`][Self::call_start()].

# Returns

[`true`] if the call succeeded, [`false`] if `error` is set.
<!-- trait ClientExt::fn call_start_sync -->
Synchronously invokes the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Client.Start">Start()`</link>` D-Bus method on `self`. The calling thread is blocked until a reply is received.

See [`call_start()`][Self::call_start()] for the asynchronous version of this method.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

[`true`] if the call succeeded, [`false`] if `error` is set.
<!-- trait ClientExt::fn call_stop -->
Asynchronously invokes the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Client.Stop">Stop()`</link>` D-Bus method on `self`.
When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
You can then call `gclue_client_call_stop_finish()` to get the result of the operation.

See [`call_stop_sync()`][Self::call_stop_sync()] for the synchronous, blocking version of this method.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the request is satisfied or [`None`].
<!-- trait ClientExt::fn call_stop_finish -->
Finishes an operation started with [`call_stop()`][Self::call_stop()].
## `res`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to [`call_stop()`][Self::call_stop()].

# Returns

[`true`] if the call succeeded, [`false`] if `error` is set.
<!-- trait ClientExt::fn call_stop_sync -->
Synchronously invokes the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Client.Stop">Stop()`</link>` D-Bus method on `self`. The calling thread is blocked until a reply is received.

See [`call_stop()`][Self::call_stop()] for the asynchronous version of this method.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

[`true`] if the call succeeded, [`false`] if `error` is set.
<!-- trait ClientExt::fn complete_start -->
Helper function used in service implementations to finish handling invocations of the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Client.Start">Start()`</link>` D-Bus method. If you instead want to finish handling an invocation by returning an error, use [`DBusMethodInvocation::return_error()`][crate::gio::DBusMethodInvocation::return_error()] or similar.

This method will free `invocation`, you cannot use it afterwards.
## `invocation`
A [`gio::DBusMethodInvocation`][crate::gio::DBusMethodInvocation].
<!-- trait ClientExt::fn complete_stop -->
Helper function used in service implementations to finish handling invocations of the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Client.Stop">Stop()`</link>` D-Bus method. If you instead want to finish handling an invocation by returning an error, use [`DBusMethodInvocation::return_error()`][crate::gio::DBusMethodInvocation::return_error()] or similar.

This method will free `invocation`, you cannot use it afterwards.
## `invocation`
A [`gio::DBusMethodInvocation`][crate::gio::DBusMethodInvocation].
<!-- trait ClientExt::fn dup_desktop_id -->
Gets a copy of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.DesktopId">"DesktopId"`</link>` D-Bus property.

Since this D-Bus property is both readable and writable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value or [`None`] if the property is not set. The returned value should be freed with `g_free()`.
<!-- trait ClientExt::fn dup_location -->
Gets a copy of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.Location">"Location"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value or [`None`] if the property is not set. The returned value should be freed with `g_free()`.
<!-- trait ClientExt::fn emit_location_updated -->
Emits the <link linkend="gdbus-signal-org-freedesktop-GeoClue2-Client.LocationUpdated">"LocationUpdated"`</link>` D-Bus signal.
## `arg_old`
Argument to pass with the signal.
## `arg_new`
Argument to pass with the signal.
<!-- trait ClientExt::fn is_active -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.Active">"Active"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value.
<!-- trait ClientExt::fn desktop_id -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.DesktopId">"DesktopId"`</link>` D-Bus property.

Since this D-Bus property is both readable and writable, it is meaningful to use this function on both the client- and service-side.

The returned value is only valid until the property changes so on the client-side it is only safe to use this function on the thread where `self` was constructed. Use [`dup_desktop_id()`][Self::dup_desktop_id()] if on another thread.

# Returns

The property value or [`None`] if the property is not set. Do not free the returned value, it belongs to `self`.
<!-- trait ClientExt::fn distance_threshold -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.DistanceThreshold">"DistanceThreshold"`</link>` D-Bus property.

Since this D-Bus property is both readable and writable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value.
<!-- trait ClientExt::fn location -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.Location">"Location"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

The returned value is only valid until the property changes so on the client-side it is only safe to use this function on the thread where `self` was constructed. Use [`dup_location()`][Self::dup_location()] if on another thread.

# Returns

The property value or [`None`] if the property is not set. Do not free the returned value, it belongs to `self`.
<!-- trait ClientExt::fn requested_accuracy_level -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.RequestedAccuracyLevel">"RequestedAccuracyLevel"`</link>` D-Bus property.

Since this D-Bus property is both readable and writable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value.
<!-- trait ClientExt::fn time_threshold -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.TimeThreshold">"TimeThreshold"`</link>` D-Bus property.

Since this D-Bus property is both readable and writable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value.
<!-- trait ClientExt::fn set_active -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.Active">"Active"`</link>` D-Bus property to `value`.

Since this D-Bus property is not writable, it is only meaningful to use this function on the service-side.
## `value`
The value to set.
<!-- trait ClientExt::fn set_desktop_id -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.DesktopId">"DesktopId"`</link>` D-Bus property to `value`.

Since this D-Bus property is both readable and writable, it is meaningful to use this function on both the client- and service-side.
## `value`
The value to set.
<!-- trait ClientExt::fn set_distance_threshold -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.DistanceThreshold">"DistanceThreshold"`</link>` D-Bus property to `value`.

Since this D-Bus property is both readable and writable, it is meaningful to use this function on both the client- and service-side.
## `value`
The value to set.
<!-- trait ClientExt::fn set_location -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.Location">"Location"`</link>` D-Bus property to `value`.

Since this D-Bus property is not writable, it is only meaningful to use this function on the service-side.
## `value`
The value to set.
<!-- trait ClientExt::fn set_requested_accuracy_level -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.RequestedAccuracyLevel">"RequestedAccuracyLevel"`</link>` D-Bus property to `value`.

Since this D-Bus property is both readable and writable, it is meaningful to use this function on both the client- and service-side.
## `value`
The value to set.
<!-- trait ClientExt::fn set_time_threshold -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.TimeThreshold">"TimeThreshold"`</link>` D-Bus property to `value`.

Since this D-Bus property is both readable and writable, it is meaningful to use this function on both the client- and service-side.
## `value`
The value to set.
<!-- trait ClientExt::fn connect_handle_start -->
Signal emitted when a remote caller is invoking the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Client.Start">Start()`</link>` D-Bus method.

If a signal handler returns [`true`], it means the signal handler will handle the invocation (e.g. take a reference to `invocation` and eventually call [`complete_start()`][Self::complete_start()] or e.g. [`DBusMethodInvocation::return_error()`][crate::gio::DBusMethodInvocation::return_error()] on it) and no order signal handlers will run. If no signal handler handles the invocation, the `G_DBUS_ERROR_UNKNOWN_METHOD` error is returned.
## `invocation`
A [`gio::DBusMethodInvocation`][crate::gio::DBusMethodInvocation].

# Returns

`G_DBUS_METHOD_INVOCATION_HANDLED` or [`true`] if the invocation was handled, `G_DBUS_METHOD_INVOCATION_UNHANDLED` or [`false`] to let other signal handlers run.
<!-- trait ClientExt::fn connect_handle_stop -->
Signal emitted when a remote caller is invoking the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Client.Stop">Stop()`</link>` D-Bus method.

If a signal handler returns [`true`], it means the signal handler will handle the invocation (e.g. take a reference to `invocation` and eventually call [`complete_stop()`][Self::complete_stop()] or e.g. [`DBusMethodInvocation::return_error()`][crate::gio::DBusMethodInvocation::return_error()] on it) and no order signal handlers will run. If no signal handler handles the invocation, the `G_DBUS_ERROR_UNKNOWN_METHOD` error is returned.
## `invocation`
A [`gio::DBusMethodInvocation`][crate::gio::DBusMethodInvocation].

# Returns

`G_DBUS_METHOD_INVOCATION_HANDLED` or [`true`] if the invocation was handled, `G_DBUS_METHOD_INVOCATION_UNHANDLED` or [`false`] to let other signal handlers run.
<!-- trait ClientExt::fn connect_location_updated -->
On the client-side, this signal is emitted whenever the D-Bus signal <link linkend="gdbus-signal-org-freedesktop-GeoClue2-Client.LocationUpdated">"LocationUpdated"`</link>` is received.

On the service-side, this signal can be used with e.g. `g_signal_emit_by_name()` to make the object emit the D-Bus signal.
## `arg_old`
Argument.
## `arg_new`
Argument.
<!-- trait ClientExt::fn active -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.Active">"Active"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- struct ClientProxy -->
The [`ClientProxy`][crate::ClientProxy] structure contains only private data and should only be accessed using the provided API.

# Implements

[`ClientExt`][trait@crate::prelude::ClientExt]
<!-- impl ClientProxyBuilder::fn g_bus_type -->
If this property is not [`gio::BusType::None`][crate::gio::BusType::None], then
`GDBusProxy:g-connection` must be [`None`] and will be set to the
[`gio::DBusConnection`][crate::gio::DBusConnection] obtained by calling `g_bus_get()` with the value
of this property.
<!-- impl ClientProxyBuilder::fn g_connection -->
The [`gio::DBusConnection`][crate::gio::DBusConnection] the proxy is for.
<!-- impl ClientProxyBuilder::fn g_default_timeout -->
The timeout to use if -1 (specifying default timeout) is passed
as `timeout_msec` in the `g_dbus_proxy_call()` and
`g_dbus_proxy_call_sync()` functions.

This allows applications to set a proxy-wide timeout for all
remote method invocations on the proxy. If this property is -1,
the default timeout (typically 25 seconds) is used. If set to
`G_MAXINT`, then no timeout is used.
<!-- impl ClientProxyBuilder::fn g_flags -->
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
<!-- impl ClientProxyBuilder::fn g_interface_info -->
Ensure that interactions with this proxy conform to the given
interface. This is mainly to ensure that malformed data received
from the other peer is ignored. The given [`gio::DBusInterfaceInfo`][crate::gio::DBusInterfaceInfo] is
said to be the "expected interface".

The checks performed are:
- When completing a method call, if the type signature of
 the reply message isn't what's expected, the reply is
 discarded and the [`glib::Error`][crate::glib::Error] is set to `G_IO_ERROR_INVALID_ARGUMENT`.

- Received signals that have a type signature mismatch are dropped and
 a warning is logged via `g_warning()`.

- Properties received via the initial `GetAll()` call or via the
 `::PropertiesChanged` signal (on the
 [org.freedesktop.DBus.Properties](http://dbus.freedesktop.org/doc/dbus-specification.html`standard`-interfaces-properties)
 interface) or set using `g_dbus_proxy_set_cached_property()`
 with a type signature mismatch are ignored and a warning is
 logged via `g_warning()`.

Note that these checks are never done on methods, signals and
properties that are not referenced in the given
[`gio::DBusInterfaceInfo`][crate::gio::DBusInterfaceInfo], since extending a D-Bus interface on the
service-side is not considered an ABI break.
<!-- impl ClientProxyBuilder::fn g_interface_name -->
The D-Bus interface name the proxy is for.
<!-- impl ClientProxyBuilder::fn g_name -->
The well-known or unique name that the proxy is for.
<!-- impl ClientProxyBuilder::fn g_name_owner -->
The unique name that owns `GDBusProxy:g-name` or [`None`] if no-one
currently owns that name. You may connect to `GObject::notify` signal to
track changes to this property.
<!-- impl ClientProxyBuilder::fn g_object_path -->
The object path the proxy is for.
<!-- impl ClientProxyBuilder::fn active -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.Active">"Active"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl ClientProxyBuilder::fn desktop_id -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.DesktopId">"DesktopId"`</link>`.

Since the D-Bus property for this `GObject` property is both readable and writable, it is meaningful to both read from it and write to it on both the service- and client-side.
<!-- impl ClientProxyBuilder::fn distance_threshold -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.DistanceThreshold">"DistanceThreshold"`</link>`.

Since the D-Bus property for this `GObject` property is both readable and writable, it is meaningful to both read from it and write to it on both the service- and client-side.
<!-- impl ClientProxyBuilder::fn location -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.Location">"Location"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl ClientProxyBuilder::fn requested_accuracy_level -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.RequestedAccuracyLevel">"RequestedAccuracyLevel"`</link>`.

Since the D-Bus property for this `GObject` property is both readable and writable, it is meaningful to both read from it and write to it on both the service- and client-side.
<!-- impl ClientProxyBuilder::fn time_threshold -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Client.TimeThreshold">"TimeThreshold"`</link>`.

Since the D-Bus property for this `GObject` property is both readable and writable, it is meaningful to both read from it and write to it on both the service- and client-side.
<!-- impl ClientProxy::fn new_finish -->
Finishes an operation started with [`new()`][Self::new()].
## `res`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to [`new()`][Self::new()].

# Returns

The constructed proxy object or [`None`] if `error` is set.
<!-- impl ClientProxy::fn for_bus_finish -->
Finishes an operation started with [`new_for_bus()`][Self::new_for_bus()].
## `res`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to [`new_for_bus()`][Self::new_for_bus()].

# Returns

The constructed proxy object or [`None`] if `error` is set.
<!-- impl ClientProxy::fn for_bus_sync -->
Like [`new_sync()`][Self::new_sync()] but takes a [`gio::BusType`][crate::gio::BusType] instead of a [`gio::DBusConnection`][crate::gio::DBusConnection].

The calling thread is blocked until a reply is received.

See [`new_for_bus()`][Self::new_for_bus()] for the asynchronous version of this constructor.
## `bus_type`
A [`gio::BusType`][crate::gio::BusType].
## `flags`
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
## `name`
A bus name (well-known or unique).
## `object_path`
An object path.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

The constructed proxy object or [`None`] if `error` is set.
<!-- impl ClientProxy::fn new_sync -->
Synchronously creates a proxy for the D-Bus interface <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Client.top_of_page">org.freedesktop.GeoClue2.Client`</link>`. See `g_dbus_proxy_new_sync()` for more details.

The calling thread is blocked until a reply is received.

See [`new()`][Self::new()] for the asynchronous version of this constructor.
## `connection`
A [`gio::DBusConnection`][crate::gio::DBusConnection].
## `flags`
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
## `name`
A bus name (well-known or unique) or [`None`] if `connection` is not a message bus connection.
## `object_path`
An object path.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

The constructed proxy object or [`None`] if `error` is set.
<!-- impl ClientProxy::fn create -->
A utility function to create a [`ClientProxy`][crate::ClientProxy] without having to deal with
a [`Manager`][crate::Manager]. See also [`create_full()`][Self::create_full()] which improves
resource management.

This is identitcal to calling [`create_full()`][Self::create_full()] without any
flags set.

See [`create_sync()`][Self::create_sync()] for the synchronous, blocking version
of this function.
## `desktop_id`
The desktop file id (the basename of the desktop file).
## `accuracy_level`
The requested accuracy level as [`AccuracyLevel`][crate::AccuracyLevel].
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the results are ready.
<!-- impl ClientProxy::fn create_finish -->
Finishes an operation started with [`create()`][Self::create()].
## `result`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to
 [`create()`][Self::create()].

# Returns

The constructed proxy
object or [`None`] if `error` is set.
<!-- impl ClientProxy::fn create_full -->
A utility function to create a [`ClientProxy`][crate::ClientProxy] without having to deal with
a [`Manager`][crate::Manager].

By setting the [`ClientProxyCreateFlags::AUTO_DELETE`][crate::ClientProxyCreateFlags::AUTO_DELETE] flag you can ensure
that the client will be deleted again from the geoclue service when
it is destroyed. This flag should be used unless you are doing explicit
resource management.

See [`create_full_sync()`][Self::create_full_sync()] for the synchronous, blocking
version of this function.
## `desktop_id`
The desktop file id (the basename of the desktop file).
## `accuracy_level`
The requested accuracy level as [`AccuracyLevel`][crate::AccuracyLevel].
## `flags`
[`ClientProxyCreateFlags`][crate::ClientProxyCreateFlags] to modify the creation.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the results are ready.
<!-- impl ClientProxy::fn create_full_finish -->
Finishes an operation started with [`create_full()`][Self::create_full()].
## `result`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to
 [`create()`][Self::create()].

# Returns

The constructed proxy
object or [`None`] if `error` is set.
<!-- impl ClientProxy::fn create_full_sync -->
The synchronous and blocking version of [`create_full()`][Self::create_full()].

By setting the [`ClientProxyCreateFlags::AUTO_DELETE`][crate::ClientProxyCreateFlags::AUTO_DELETE] flag you can ensure
that the client will be deleted again from the geoclue service when
it is destroyed. This flag should be used unless you are doing explicit
resource management.
## `desktop_id`
The desktop file id (the basename of the desktop file).
## `accuracy_level`
The requested accuracy level as [`AccuracyLevel`][crate::AccuracyLevel].
## `flags`
[`ClientProxyCreateFlags`][crate::ClientProxyCreateFlags] to modify the creation.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

The constructed proxy
object or [`None`] if `error` is set.
<!-- impl ClientProxy::fn create_sync -->
The synchronous and blocking version of [`create()`][Self::create()].
See also [`create_full_sync()`][Self::create_full_sync()] which improves resource
management.

This function is identical to calling [`create_full_sync()`][Self::create_full_sync()]
without any flags set.
## `desktop_id`
The desktop file id (the basename of the desktop file).
## `accuracy_level`
The requested accuracy level as [`AccuracyLevel`][crate::AccuracyLevel].
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

The constructed proxy
object or [`None`] if `error` is set.
<!-- impl ClientProxy::fn new -->
Asynchronously creates a proxy for the D-Bus interface <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Client.top_of_page">org.freedesktop.GeoClue2.Client`</link>`. See `g_dbus_proxy_new()` for more details.

When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
You can then call `gclue_client_proxy_new_finish()` to get the result of the operation.

See [`new_sync()`][Self::new_sync()] for the synchronous, blocking version of this constructor.
## `connection`
A [`gio::DBusConnection`][crate::gio::DBusConnection].
## `flags`
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
## `name`
A bus name (well-known or unique) or [`None`] if `connection` is not a message bus connection.
## `object_path`
An object path.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the request is satisfied.
<!-- impl ClientProxy::fn new_for_bus -->
Like [`new()`][Self::new()] but takes a [`gio::BusType`][crate::gio::BusType] instead of a [`gio::DBusConnection`][crate::gio::DBusConnection].

When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
You can then call `gclue_client_proxy_new_for_bus_finish()` to get the result of the operation.

See [`for_bus_sync()`][Self::for_bus_sync()] for the synchronous, blocking version of this constructor.
## `bus_type`
A [`gio::BusType`][crate::gio::BusType].
## `flags`
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
## `name`
A bus name (well-known or unique).
## `object_path`
An object path.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the request is satisfied.
<!-- struct ClientProxyCreateFlags -->
<!-- struct Location -->
Abstract interface type for the D-Bus interface <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Location.top_of_page">org.freedesktop.GeoClue2.Location`</link>`.

# Implements

[`LocationExt`][trait@crate::prelude::LocationExt]
<!-- trait LocationExt -->
Trait containing all [`struct@Location`] methods.

# Implementors

[`LocationProxy`][struct@crate::LocationProxy], [`LocationSkeleton`][struct@crate::LocationSkeleton], [`Location`][struct@crate::Location]
<!-- impl Location::fn interface_info -->
Gets a machine-readable description of the <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Location.top_of_page">org.freedesktop.GeoClue2.Location`</link>` D-Bus interface.

# Returns

A [`gio::DBusInterfaceInfo`][crate::gio::DBusInterfaceInfo]. Do not free.
<!-- impl Location::fn override_properties -->
Overrides all `GObject` properties in the [`Location`][crate::Location] interface for a concrete class.
The properties are overridden in the order they are defined.
## `klass`
The class structure for a `GObject` derived class.
## `property_id_begin`
The property id to assign to the first overridden property.

# Returns

The last property id.
<!-- trait LocationExt::fn dup_description -->
Gets a copy of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Description">"Description"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value or [`None`] if the property is not set. The returned value should be freed with `g_free()`.
<!-- trait LocationExt::fn dup_timestamp -->
Gets a copy of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Timestamp">"Timestamp"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value or [`None`] if the property is not set. The returned value should be freed with `g_variant_unref()`.
<!-- trait LocationExt::fn accuracy -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Accuracy">"Accuracy"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value.
<!-- trait LocationExt::fn altitude -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Altitude">"Altitude"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value.
<!-- trait LocationExt::fn description -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Description">"Description"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

The returned value is only valid until the property changes so on the client-side it is only safe to use this function on the thread where `self` was constructed. Use [`dup_description()`][Self::dup_description()] if on another thread.

# Returns

The property value or [`None`] if the property is not set. Do not free the returned value, it belongs to `self`.
<!-- trait LocationExt::fn heading -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Heading">"Heading"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value.
<!-- trait LocationExt::fn latitude -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Latitude">"Latitude"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value.
<!-- trait LocationExt::fn longitude -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Longitude">"Longitude"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value.
<!-- trait LocationExt::fn speed -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Speed">"Speed"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value.
<!-- trait LocationExt::fn timestamp -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Timestamp">"Timestamp"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

The returned value is only valid until the property changes so on the client-side it is only safe to use this function on the thread where `self` was constructed. Use [`dup_timestamp()`][Self::dup_timestamp()] if on another thread.

# Returns

The property value or [`None`] if the property is not set. Do not free the returned value, it belongs to `self`.
<!-- trait LocationExt::fn set_accuracy -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Accuracy">"Accuracy"`</link>` D-Bus property to `value`.

Since this D-Bus property is not writable, it is only meaningful to use this function on the service-side.
## `value`
The value to set.
<!-- trait LocationExt::fn set_altitude -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Altitude">"Altitude"`</link>` D-Bus property to `value`.

Since this D-Bus property is not writable, it is only meaningful to use this function on the service-side.
## `value`
The value to set.
<!-- trait LocationExt::fn set_description -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Description">"Description"`</link>` D-Bus property to `value`.

Since this D-Bus property is not writable, it is only meaningful to use this function on the service-side.
## `value`
The value to set.
<!-- trait LocationExt::fn set_heading -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Heading">"Heading"`</link>` D-Bus property to `value`.

Since this D-Bus property is not writable, it is only meaningful to use this function on the service-side.
## `value`
The value to set.
<!-- trait LocationExt::fn set_latitude -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Latitude">"Latitude"`</link>` D-Bus property to `value`.

Since this D-Bus property is not writable, it is only meaningful to use this function on the service-side.
## `value`
The value to set.
<!-- trait LocationExt::fn set_longitude -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Longitude">"Longitude"`</link>` D-Bus property to `value`.

Since this D-Bus property is not writable, it is only meaningful to use this function on the service-side.
## `value`
The value to set.
<!-- trait LocationExt::fn set_speed -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Speed">"Speed"`</link>` D-Bus property to `value`.

Since this D-Bus property is not writable, it is only meaningful to use this function on the service-side.
## `value`
The value to set.
<!-- trait LocationExt::fn set_timestamp -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Timestamp">"Timestamp"`</link>` D-Bus property to `value`.

Since this D-Bus property is not writable, it is only meaningful to use this function on the service-side.
## `value`
The value to set.
<!-- struct LocationProxy -->
The [`LocationProxy`][crate::LocationProxy] structure contains only private data and should only be accessed using the provided API.

# Implements

[`LocationExt`][trait@crate::prelude::LocationExt]
<!-- impl LocationProxyBuilder::fn g_bus_type -->
If this property is not [`gio::BusType::None`][crate::gio::BusType::None], then
`GDBusProxy:g-connection` must be [`None`] and will be set to the
[`gio::DBusConnection`][crate::gio::DBusConnection] obtained by calling `g_bus_get()` with the value
of this property.
<!-- impl LocationProxyBuilder::fn g_connection -->
The [`gio::DBusConnection`][crate::gio::DBusConnection] the proxy is for.
<!-- impl LocationProxyBuilder::fn g_default_timeout -->
The timeout to use if -1 (specifying default timeout) is passed
as `timeout_msec` in the `g_dbus_proxy_call()` and
`g_dbus_proxy_call_sync()` functions.

This allows applications to set a proxy-wide timeout for all
remote method invocations on the proxy. If this property is -1,
the default timeout (typically 25 seconds) is used. If set to
`G_MAXINT`, then no timeout is used.
<!-- impl LocationProxyBuilder::fn g_flags -->
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
<!-- impl LocationProxyBuilder::fn g_interface_info -->
Ensure that interactions with this proxy conform to the given
interface. This is mainly to ensure that malformed data received
from the other peer is ignored. The given [`gio::DBusInterfaceInfo`][crate::gio::DBusInterfaceInfo] is
said to be the "expected interface".

The checks performed are:
- When completing a method call, if the type signature of
 the reply message isn't what's expected, the reply is
 discarded and the [`glib::Error`][crate::glib::Error] is set to `G_IO_ERROR_INVALID_ARGUMENT`.

- Received signals that have a type signature mismatch are dropped and
 a warning is logged via `g_warning()`.

- Properties received via the initial `GetAll()` call or via the
 `::PropertiesChanged` signal (on the
 [org.freedesktop.DBus.Properties](http://dbus.freedesktop.org/doc/dbus-specification.html`standard`-interfaces-properties)
 interface) or set using `g_dbus_proxy_set_cached_property()`
 with a type signature mismatch are ignored and a warning is
 logged via `g_warning()`.

Note that these checks are never done on methods, signals and
properties that are not referenced in the given
[`gio::DBusInterfaceInfo`][crate::gio::DBusInterfaceInfo], since extending a D-Bus interface on the
service-side is not considered an ABI break.
<!-- impl LocationProxyBuilder::fn g_interface_name -->
The D-Bus interface name the proxy is for.
<!-- impl LocationProxyBuilder::fn g_name -->
The well-known or unique name that the proxy is for.
<!-- impl LocationProxyBuilder::fn g_name_owner -->
The unique name that owns `GDBusProxy:g-name` or [`None`] if no-one
currently owns that name. You may connect to `GObject::notify` signal to
track changes to this property.
<!-- impl LocationProxyBuilder::fn g_object_path -->
The object path the proxy is for.
<!-- impl LocationProxyBuilder::fn accuracy -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Accuracy">"Accuracy"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationProxyBuilder::fn altitude -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Altitude">"Altitude"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationProxyBuilder::fn description -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Description">"Description"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationProxyBuilder::fn heading -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Heading">"Heading"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationProxyBuilder::fn latitude -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Latitude">"Latitude"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationProxyBuilder::fn longitude -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Longitude">"Longitude"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationProxyBuilder::fn speed -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Speed">"Speed"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationProxyBuilder::fn timestamp -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Timestamp">"Timestamp"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationProxy::fn new_finish -->
Finishes an operation started with [`new()`][Self::new()].
## `res`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to [`new()`][Self::new()].

# Returns

The constructed proxy object or [`None`] if `error` is set.
<!-- impl LocationProxy::fn for_bus_finish -->
Finishes an operation started with [`new_for_bus()`][Self::new_for_bus()].
## `res`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to [`new_for_bus()`][Self::new_for_bus()].

# Returns

The constructed proxy object or [`None`] if `error` is set.
<!-- impl LocationProxy::fn for_bus_sync -->
Like [`new_sync()`][Self::new_sync()] but takes a [`gio::BusType`][crate::gio::BusType] instead of a [`gio::DBusConnection`][crate::gio::DBusConnection].

The calling thread is blocked until a reply is received.

See [`new_for_bus()`][Self::new_for_bus()] for the asynchronous version of this constructor.
## `bus_type`
A [`gio::BusType`][crate::gio::BusType].
## `flags`
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
## `name`
A bus name (well-known or unique).
## `object_path`
An object path.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

The constructed proxy object or [`None`] if `error` is set.
<!-- impl LocationProxy::fn new_sync -->
Synchronously creates a proxy for the D-Bus interface <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Location.top_of_page">org.freedesktop.GeoClue2.Location`</link>`. See `g_dbus_proxy_new_sync()` for more details.

The calling thread is blocked until a reply is received.

See [`new()`][Self::new()] for the asynchronous version of this constructor.
## `connection`
A [`gio::DBusConnection`][crate::gio::DBusConnection].
## `flags`
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
## `name`
A bus name (well-known or unique) or [`None`] if `connection` is not a message bus connection.
## `object_path`
An object path.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

The constructed proxy object or [`None`] if `error` is set.
<!-- impl LocationProxy::fn new -->
Asynchronously creates a proxy for the D-Bus interface <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Location.top_of_page">org.freedesktop.GeoClue2.Location`</link>`. See `g_dbus_proxy_new()` for more details.

When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
You can then call `gclue_location_proxy_new_finish()` to get the result of the operation.

See [`new_sync()`][Self::new_sync()] for the synchronous, blocking version of this constructor.
## `connection`
A [`gio::DBusConnection`][crate::gio::DBusConnection].
## `flags`
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
## `name`
A bus name (well-known or unique) or [`None`] if `connection` is not a message bus connection.
## `object_path`
An object path.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the request is satisfied.
<!-- impl LocationProxy::fn new_for_bus -->
Like [`new()`][Self::new()] but takes a [`gio::BusType`][crate::gio::BusType] instead of a [`gio::DBusConnection`][crate::gio::DBusConnection].

When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
You can then call `gclue_location_proxy_new_for_bus_finish()` to get the result of the operation.

See [`for_bus_sync()`][Self::for_bus_sync()] for the synchronous, blocking version of this constructor.
## `bus_type`
A [`gio::BusType`][crate::gio::BusType].
## `flags`
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
## `name`
A bus name (well-known or unique).
## `object_path`
An object path.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the request is satisfied.
<!-- struct LocationSkeleton -->
The [`LocationSkeleton`][crate::LocationSkeleton] structure contains only private data and should only be accessed using the provided API.

# Implements

[`LocationExt`][trait@crate::prelude::LocationExt]
<!-- impl LocationSkeletonBuilder::fn g_flags -->
Flags from the `GDBusInterfaceSkeletonFlags` enumeration.
<!-- impl LocationSkeletonBuilder::fn accuracy -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Accuracy">"Accuracy"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationSkeletonBuilder::fn altitude -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Altitude">"Altitude"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationSkeletonBuilder::fn description -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Description">"Description"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationSkeletonBuilder::fn heading -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Heading">"Heading"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationSkeletonBuilder::fn latitude -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Latitude">"Latitude"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationSkeletonBuilder::fn longitude -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Longitude">"Longitude"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationSkeletonBuilder::fn speed -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Speed">"Speed"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationSkeletonBuilder::fn timestamp -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Location.Timestamp">"Timestamp"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl LocationSkeleton::fn new -->
Creates a skeleton object for the D-Bus interface <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Location.top_of_page">org.freedesktop.GeoClue2.Location`</link>`.

# Returns

The skeleton object.
<!-- struct Manager -->
Abstract interface type for the D-Bus interface <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Manager.top_of_page">org.freedesktop.GeoClue2.Manager`</link>`.

# Implements

[`ManagerExt`][trait@crate::prelude::ManagerExt]
<!-- trait ManagerExt -->
Trait containing all [`struct@Manager`] methods.

# Implementors

[`ManagerProxy`][struct@crate::ManagerProxy], [`ManagerSkeleton`][struct@crate::ManagerSkeleton], [`Manager`][struct@crate::Manager]
<!-- impl Manager::fn interface_info -->
Gets a machine-readable description of the <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Manager.top_of_page">org.freedesktop.GeoClue2.Manager`</link>` D-Bus interface.

# Returns

A [`gio::DBusInterfaceInfo`][crate::gio::DBusInterfaceInfo]. Do not free.
<!-- impl Manager::fn override_properties -->
Overrides all `GObject` properties in the [`Manager`][crate::Manager] interface for a concrete class.
The properties are overridden in the order they are defined.
## `klass`
The class structure for a `GObject` derived class.
## `property_id_begin`
The property id to assign to the first overridden property.

# Returns

The last property id.
<!-- trait ManagerExt::fn call_add_agent -->
Asynchronously invokes the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.AddAgent">AddAgent()`</link>` D-Bus method on `self`.
When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
You can then call `gclue_manager_call_add_agent_finish()` to get the result of the operation.

See [`call_add_agent_sync()`][Self::call_add_agent_sync()] for the synchronous, blocking version of this method.
## `arg_id`
Argument to pass with the method invocation.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the request is satisfied or [`None`].
<!-- trait ManagerExt::fn call_add_agent_finish -->
Finishes an operation started with [`call_add_agent()`][Self::call_add_agent()].
## `res`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to [`call_add_agent()`][Self::call_add_agent()].

# Returns

[`true`] if the call succeeded, [`false`] if `error` is set.
<!-- trait ManagerExt::fn call_add_agent_sync -->
Synchronously invokes the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.AddAgent">AddAgent()`</link>` D-Bus method on `self`. The calling thread is blocked until a reply is received.

See [`call_add_agent()`][Self::call_add_agent()] for the asynchronous version of this method.
## `arg_id`
Argument to pass with the method invocation.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

[`true`] if the call succeeded, [`false`] if `error` is set.
<!-- trait ManagerExt::fn call_create_client -->
Asynchronously invokes the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.CreateClient">CreateClient()`</link>` D-Bus method on `self`.
When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
You can then call `gclue_manager_call_create_client_finish()` to get the result of the operation.

See [`call_create_client_sync()`][Self::call_create_client_sync()] for the synchronous, blocking version of this method.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the request is satisfied or [`None`].
<!-- trait ManagerExt::fn call_create_client_finish -->
Finishes an operation started with [`call_create_client()`][Self::call_create_client()].
## `res`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to [`call_create_client()`][Self::call_create_client()].

# Returns

[`true`] if the call succeeded, [`false`] if `error` is set.

## `out_client`
Return location for return parameter or [`None`] to ignore.
<!-- trait ManagerExt::fn call_create_client_sync -->
Synchronously invokes the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.CreateClient">CreateClient()`</link>` D-Bus method on `self`. The calling thread is blocked until a reply is received.

See [`call_create_client()`][Self::call_create_client()] for the asynchronous version of this method.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

[`true`] if the call succeeded, [`false`] if `error` is set.

## `out_client`
Return location for return parameter or [`None`] to ignore.
<!-- trait ManagerExt::fn call_delete_client -->
Asynchronously invokes the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.DeleteClient">DeleteClient()`</link>` D-Bus method on `self`.
When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
You can then call `gclue_manager_call_delete_client_finish()` to get the result of the operation.

See [`call_delete_client_sync()`][Self::call_delete_client_sync()] for the synchronous, blocking version of this method.
## `arg_client`
Argument to pass with the method invocation.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the request is satisfied or [`None`].
<!-- trait ManagerExt::fn call_delete_client_finish -->
Finishes an operation started with [`call_delete_client()`][Self::call_delete_client()].
## `res`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to [`call_delete_client()`][Self::call_delete_client()].

# Returns

[`true`] if the call succeeded, [`false`] if `error` is set.
<!-- trait ManagerExt::fn call_delete_client_sync -->
Synchronously invokes the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.DeleteClient">DeleteClient()`</link>` D-Bus method on `self`. The calling thread is blocked until a reply is received.

See [`call_delete_client()`][Self::call_delete_client()] for the asynchronous version of this method.
## `arg_client`
Argument to pass with the method invocation.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

[`true`] if the call succeeded, [`false`] if `error` is set.
<!-- trait ManagerExt::fn call_get_client -->
Asynchronously invokes the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.GetClient">GetClient()`</link>` D-Bus method on `self`.
When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
You can then call `gclue_manager_call_get_client_finish()` to get the result of the operation.

See [`call_get_client_sync()`][Self::call_get_client_sync()] for the synchronous, blocking version of this method.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the request is satisfied or [`None`].
<!-- trait ManagerExt::fn call_get_client_finish -->
Finishes an operation started with [`call_get_client()`][Self::call_get_client()].
## `res`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to [`call_get_client()`][Self::call_get_client()].

# Returns

[`true`] if the call succeeded, [`false`] if `error` is set.

## `out_client`
Return location for return parameter or [`None`] to ignore.
<!-- trait ManagerExt::fn call_get_client_sync -->
Synchronously invokes the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.GetClient">GetClient()`</link>` D-Bus method on `self`. The calling thread is blocked until a reply is received.

See [`call_get_client()`][Self::call_get_client()] for the asynchronous version of this method.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

[`true`] if the call succeeded, [`false`] if `error` is set.

## `out_client`
Return location for return parameter or [`None`] to ignore.
<!-- trait ManagerExt::fn complete_add_agent -->
Helper function used in service implementations to finish handling invocations of the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.AddAgent">AddAgent()`</link>` D-Bus method. If you instead want to finish handling an invocation by returning an error, use [`DBusMethodInvocation::return_error()`][crate::gio::DBusMethodInvocation::return_error()] or similar.

This method will free `invocation`, you cannot use it afterwards.
## `invocation`
A [`gio::DBusMethodInvocation`][crate::gio::DBusMethodInvocation].
<!-- trait ManagerExt::fn complete_create_client -->
Helper function used in service implementations to finish handling invocations of the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.CreateClient">CreateClient()`</link>` D-Bus method. If you instead want to finish handling an invocation by returning an error, use [`DBusMethodInvocation::return_error()`][crate::gio::DBusMethodInvocation::return_error()] or similar.

This method will free `invocation`, you cannot use it afterwards.
## `invocation`
A [`gio::DBusMethodInvocation`][crate::gio::DBusMethodInvocation].
## `client`
Parameter to return.
<!-- trait ManagerExt::fn complete_delete_client -->
Helper function used in service implementations to finish handling invocations of the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.DeleteClient">DeleteClient()`</link>` D-Bus method. If you instead want to finish handling an invocation by returning an error, use [`DBusMethodInvocation::return_error()`][crate::gio::DBusMethodInvocation::return_error()] or similar.

This method will free `invocation`, you cannot use it afterwards.
## `invocation`
A [`gio::DBusMethodInvocation`][crate::gio::DBusMethodInvocation].
<!-- trait ManagerExt::fn complete_get_client -->
Helper function used in service implementations to finish handling invocations of the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.GetClient">GetClient()`</link>` D-Bus method. If you instead want to finish handling an invocation by returning an error, use [`DBusMethodInvocation::return_error()`][crate::gio::DBusMethodInvocation::return_error()] or similar.

This method will free `invocation`, you cannot use it afterwards.
## `invocation`
A [`gio::DBusMethodInvocation`][crate::gio::DBusMethodInvocation].
## `client`
Parameter to return.
<!-- trait ManagerExt::fn available_accuracy_level -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Manager.AvailableAccuracyLevel">"AvailableAccuracyLevel"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value.
<!-- trait ManagerExt::fn is_in_use -->
Gets the value of the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Manager.InUse">"InUse"`</link>` D-Bus property.

Since this D-Bus property is readable, it is meaningful to use this function on both the client- and service-side.

# Returns

The property value.
<!-- trait ManagerExt::fn set_available_accuracy_level -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Manager.AvailableAccuracyLevel">"AvailableAccuracyLevel"`</link>` D-Bus property to `value`.

Since this D-Bus property is not writable, it is only meaningful to use this function on the service-side.
## `value`
The value to set.
<!-- trait ManagerExt::fn set_in_use -->
Sets the <link linkend="gdbus-property-org-freedesktop-GeoClue2-Manager.InUse">"InUse"`</link>` D-Bus property to `value`.

Since this D-Bus property is not writable, it is only meaningful to use this function on the service-side.
## `value`
The value to set.
<!-- trait ManagerExt::fn connect_handle_add_agent -->
Signal emitted when a remote caller is invoking the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.AddAgent">AddAgent()`</link>` D-Bus method.

If a signal handler returns [`true`], it means the signal handler will handle the invocation (e.g. take a reference to `invocation` and eventually call [`complete_add_agent()`][Self::complete_add_agent()] or e.g. [`DBusMethodInvocation::return_error()`][crate::gio::DBusMethodInvocation::return_error()] on it) and no order signal handlers will run. If no signal handler handles the invocation, the `G_DBUS_ERROR_UNKNOWN_METHOD` error is returned.
## `invocation`
A [`gio::DBusMethodInvocation`][crate::gio::DBusMethodInvocation].
## `arg_id`
Argument passed by remote caller.

# Returns

`G_DBUS_METHOD_INVOCATION_HANDLED` or [`true`] if the invocation was handled, `G_DBUS_METHOD_INVOCATION_UNHANDLED` or [`false`] to let other signal handlers run.
<!-- trait ManagerExt::fn connect_handle_create_client -->
Signal emitted when a remote caller is invoking the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.CreateClient">CreateClient()`</link>` D-Bus method.

If a signal handler returns [`true`], it means the signal handler will handle the invocation (e.g. take a reference to `invocation` and eventually call [`complete_create_client()`][Self::complete_create_client()] or e.g. [`DBusMethodInvocation::return_error()`][crate::gio::DBusMethodInvocation::return_error()] on it) and no order signal handlers will run. If no signal handler handles the invocation, the `G_DBUS_ERROR_UNKNOWN_METHOD` error is returned.
## `invocation`
A [`gio::DBusMethodInvocation`][crate::gio::DBusMethodInvocation].

# Returns

`G_DBUS_METHOD_INVOCATION_HANDLED` or [`true`] if the invocation was handled, `G_DBUS_METHOD_INVOCATION_UNHANDLED` or [`false`] to let other signal handlers run.
<!-- trait ManagerExt::fn connect_handle_delete_client -->
Signal emitted when a remote caller is invoking the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.DeleteClient">DeleteClient()`</link>` D-Bus method.

If a signal handler returns [`true`], it means the signal handler will handle the invocation (e.g. take a reference to `invocation` and eventually call [`complete_delete_client()`][Self::complete_delete_client()] or e.g. [`DBusMethodInvocation::return_error()`][crate::gio::DBusMethodInvocation::return_error()] on it) and no order signal handlers will run. If no signal handler handles the invocation, the `G_DBUS_ERROR_UNKNOWN_METHOD` error is returned.
## `invocation`
A [`gio::DBusMethodInvocation`][crate::gio::DBusMethodInvocation].
## `arg_client`
Argument passed by remote caller.

# Returns

`G_DBUS_METHOD_INVOCATION_HANDLED` or [`true`] if the invocation was handled, `G_DBUS_METHOD_INVOCATION_UNHANDLED` or [`false`] to let other signal handlers run.
<!-- trait ManagerExt::fn connect_handle_get_client -->
Signal emitted when a remote caller is invoking the <link linkend="gdbus-method-org-freedesktop-GeoClue2-Manager.GetClient">GetClient()`</link>` D-Bus method.

If a signal handler returns [`true`], it means the signal handler will handle the invocation (e.g. take a reference to `invocation` and eventually call [`complete_get_client()`][Self::complete_get_client()] or e.g. [`DBusMethodInvocation::return_error()`][crate::gio::DBusMethodInvocation::return_error()] on it) and no order signal handlers will run. If no signal handler handles the invocation, the `G_DBUS_ERROR_UNKNOWN_METHOD` error is returned.
## `invocation`
A [`gio::DBusMethodInvocation`][crate::gio::DBusMethodInvocation].

# Returns

`G_DBUS_METHOD_INVOCATION_HANDLED` or [`true`] if the invocation was handled, `G_DBUS_METHOD_INVOCATION_UNHANDLED` or [`false`] to let other signal handlers run.
<!-- trait ManagerExt::fn in_use -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Manager.InUse">"InUse"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- struct ManagerProxy -->
The [`ManagerProxy`][crate::ManagerProxy] structure contains only private data and should only be accessed using the provided API.

# Implements

[`ManagerExt`][trait@crate::prelude::ManagerExt]
<!-- impl ManagerProxyBuilder::fn g_bus_type -->
If this property is not [`gio::BusType::None`][crate::gio::BusType::None], then
`GDBusProxy:g-connection` must be [`None`] and will be set to the
[`gio::DBusConnection`][crate::gio::DBusConnection] obtained by calling `g_bus_get()` with the value
of this property.
<!-- impl ManagerProxyBuilder::fn g_connection -->
The [`gio::DBusConnection`][crate::gio::DBusConnection] the proxy is for.
<!-- impl ManagerProxyBuilder::fn g_default_timeout -->
The timeout to use if -1 (specifying default timeout) is passed
as `timeout_msec` in the `g_dbus_proxy_call()` and
`g_dbus_proxy_call_sync()` functions.

This allows applications to set a proxy-wide timeout for all
remote method invocations on the proxy. If this property is -1,
the default timeout (typically 25 seconds) is used. If set to
`G_MAXINT`, then no timeout is used.
<!-- impl ManagerProxyBuilder::fn g_flags -->
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
<!-- impl ManagerProxyBuilder::fn g_interface_info -->
Ensure that interactions with this proxy conform to the given
interface. This is mainly to ensure that malformed data received
from the other peer is ignored. The given [`gio::DBusInterfaceInfo`][crate::gio::DBusInterfaceInfo] is
said to be the "expected interface".

The checks performed are:
- When completing a method call, if the type signature of
 the reply message isn't what's expected, the reply is
 discarded and the [`glib::Error`][crate::glib::Error] is set to `G_IO_ERROR_INVALID_ARGUMENT`.

- Received signals that have a type signature mismatch are dropped and
 a warning is logged via `g_warning()`.

- Properties received via the initial `GetAll()` call or via the
 `::PropertiesChanged` signal (on the
 [org.freedesktop.DBus.Properties](http://dbus.freedesktop.org/doc/dbus-specification.html`standard`-interfaces-properties)
 interface) or set using `g_dbus_proxy_set_cached_property()`
 with a type signature mismatch are ignored and a warning is
 logged via `g_warning()`.

Note that these checks are never done on methods, signals and
properties that are not referenced in the given
[`gio::DBusInterfaceInfo`][crate::gio::DBusInterfaceInfo], since extending a D-Bus interface on the
service-side is not considered an ABI break.
<!-- impl ManagerProxyBuilder::fn g_interface_name -->
The D-Bus interface name the proxy is for.
<!-- impl ManagerProxyBuilder::fn g_name -->
The well-known or unique name that the proxy is for.
<!-- impl ManagerProxyBuilder::fn g_name_owner -->
The unique name that owns `GDBusProxy:g-name` or [`None`] if no-one
currently owns that name. You may connect to `GObject::notify` signal to
track changes to this property.
<!-- impl ManagerProxyBuilder::fn g_object_path -->
The object path the proxy is for.
<!-- impl ManagerProxyBuilder::fn available_accuracy_level -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Manager.AvailableAccuracyLevel">"AvailableAccuracyLevel"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl ManagerProxyBuilder::fn in_use -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Manager.InUse">"InUse"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl ManagerProxy::fn new_finish -->
Finishes an operation started with [`new()`][Self::new()].
## `res`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to [`new()`][Self::new()].

# Returns

The constructed proxy object or [`None`] if `error` is set.
<!-- impl ManagerProxy::fn for_bus_finish -->
Finishes an operation started with [`new_for_bus()`][Self::new_for_bus()].
## `res`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to [`new_for_bus()`][Self::new_for_bus()].

# Returns

The constructed proxy object or [`None`] if `error` is set.
<!-- impl ManagerProxy::fn for_bus_sync -->
Like [`new_sync()`][Self::new_sync()] but takes a [`gio::BusType`][crate::gio::BusType] instead of a [`gio::DBusConnection`][crate::gio::DBusConnection].

The calling thread is blocked until a reply is received.

See [`new_for_bus()`][Self::new_for_bus()] for the asynchronous version of this constructor.
## `bus_type`
A [`gio::BusType`][crate::gio::BusType].
## `flags`
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
## `name`
A bus name (well-known or unique).
## `object_path`
An object path.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

The constructed proxy object or [`None`] if `error` is set.
<!-- impl ManagerProxy::fn new_sync -->
Synchronously creates a proxy for the D-Bus interface <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Manager.top_of_page">org.freedesktop.GeoClue2.Manager`</link>`. See `g_dbus_proxy_new_sync()` for more details.

The calling thread is blocked until a reply is received.

See [`new()`][Self::new()] for the asynchronous version of this constructor.
## `connection`
A [`gio::DBusConnection`][crate::gio::DBusConnection].
## `flags`
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
## `name`
A bus name (well-known or unique) or [`None`] if `connection` is not a message bus connection.
## `object_path`
An object path.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

The constructed proxy object or [`None`] if `error` is set.
<!-- impl ManagerProxy::fn new -->
Asynchronously creates a proxy for the D-Bus interface <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Manager.top_of_page">org.freedesktop.GeoClue2.Manager`</link>`. See `g_dbus_proxy_new()` for more details.

When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
You can then call `gclue_manager_proxy_new_finish()` to get the result of the operation.

See [`new_sync()`][Self::new_sync()] for the synchronous, blocking version of this constructor.
## `connection`
A [`gio::DBusConnection`][crate::gio::DBusConnection].
## `flags`
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
## `name`
A bus name (well-known or unique) or [`None`] if `connection` is not a message bus connection.
## `object_path`
An object path.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the request is satisfied.
<!-- impl ManagerProxy::fn new_for_bus -->
Like [`new()`][Self::new()] but takes a [`gio::BusType`][crate::gio::BusType] instead of a [`gio::DBusConnection`][crate::gio::DBusConnection].

When the operation is finished, `callback` will be invoked in the thread-default main loop of the thread you are calling this method from (see `g_main_context_push_thread_default()`).
You can then call `gclue_manager_proxy_new_for_bus_finish()` to get the result of the operation.

See [`for_bus_sync()`][Self::for_bus_sync()] for the synchronous, blocking version of this constructor.
## `bus_type`
A [`gio::BusType`][crate::gio::BusType].
## `flags`
Flags from the [`gio::DBusProxyFlags`][crate::gio::DBusProxyFlags] enumeration.
## `name`
A bus name (well-known or unique).
## `object_path`
An object path.
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the request is satisfied.
<!-- struct ManagerSkeleton -->
The [`ManagerSkeleton`][crate::ManagerSkeleton] structure contains only private data and should only be accessed using the provided API.

# Implements

[`ManagerExt`][trait@crate::prelude::ManagerExt]
<!-- impl ManagerSkeletonBuilder::fn g_flags -->
Flags from the `GDBusInterfaceSkeletonFlags` enumeration.
<!-- impl ManagerSkeletonBuilder::fn available_accuracy_level -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Manager.AvailableAccuracyLevel">"AvailableAccuracyLevel"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl ManagerSkeletonBuilder::fn in_use -->
Represents the D-Bus property <link linkend="gdbus-property-org-freedesktop-GeoClue2-Manager.InUse">"InUse"`</link>`.

Since the D-Bus property for this `GObject` property is readable but not writable, it is meaningful to read from it on both the client- and service-side. It is only meaningful, however, to write to it on the service-side.
<!-- impl ManagerSkeleton::fn new -->
Creates a skeleton object for the D-Bus interface <link linkend="gdbus-interface-org-freedesktop-GeoClue2-Manager.top_of_page">org.freedesktop.GeoClue2.Manager`</link>`.

# Returns

The skeleton object.
<!-- struct Simple -->


# Implements

[`SimpleExt`][trait@crate::prelude::SimpleExt]
<!-- impl SimpleBuilder::fn accuracy_level -->
The requested maximum accuracy level.
<!-- impl SimpleBuilder::fn client -->
The client proxy.
<!-- impl SimpleBuilder::fn desktop_id -->
The Desktop ID of the application.
<!-- impl SimpleBuilder::fn location -->
The current location.
<!-- trait SimpleExt -->
Trait containing all [`struct@Simple`] methods.

# Implementors

[`Simple`][struct@crate::Simple]
<!-- impl Simple::fn new_finish -->
Finishes an operation started with [`new()`][Self::new()].
## `result`
The [`gio::AsyncResult`][crate::gio::AsyncResult] obtained from the `GAsyncReadyCallback` passed to
 [`new()`][Self::new()].

# Returns

The constructed proxy
object or [`None`] if `error` is set.
<!-- impl Simple::fn new_sync -->
The synchronous and blocking version of [`new()`][Self::new()].
## `desktop_id`
The desktop file id (the basename of the desktop file).
## `accuracy_level`
The requested accuracy level as [`AccuracyLevel`][crate::AccuracyLevel].
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].

# Returns

The new [`Simple`][crate::Simple] object or
[`None`] if `error` is set.
<!-- impl Simple::fn new -->
Asynchronously creates a [`Simple`][crate::Simple] instance. Use
`gclue_simple_new_finish()` to get the created [`Simple`][crate::Simple] instance.

See [`new_sync()`][Self::new_sync()] for the synchronous, blocking version
of this function.
## `desktop_id`
The desktop file id (the basename of the desktop file).
## `accuracy_level`
The requested accuracy level as [`AccuracyLevel`][crate::AccuracyLevel].
## `cancellable`
A [`gio::Cancellable`][crate::gio::Cancellable] or [`None`].
## `callback`
A `GAsyncReadyCallback` to call when the results are ready.
<!-- trait SimpleExt::fn client -->
Gets the client proxy.

# Returns

The client object.
<!-- trait SimpleExt::fn location -->
Gets the current location.

# Returns

The last known location
as [`Location`][crate::Location].
<!-- trait SimpleExt::fn set_accuracy_level -->
The requested maximum accuracy level.
<!-- trait SimpleExt::fn set_desktop_id -->
The Desktop ID of the application.
